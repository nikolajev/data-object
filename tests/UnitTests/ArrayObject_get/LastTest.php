<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_get;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class LastTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals('me', Data::array(['test', 'me'])->_get()->last());
    }

    public function testNewInstance(): void
    {
        $array = [
            'test1' => ['me1'],
            'test2' => ['me2'],
        ];

        $this->assertEquals((new ArrayObject(['me2'])), Data::array($array)->_get()->last());
    }

    public function testArray(): void
    {
        $array = [
            'test1' => ['me1'],
            'test2' => ['me2'],
        ];

        $this->assertEquals(['me2'], Data::array($array)->_get()->last(false));
    }

    public function testNewInstanceWithSelector(): void
    {
        $array = [
            'test1' => ['me1', 'please1'],
            'test2' => ['me2'],
        ];

        $this->assertEquals('please1', Data::array($array)->select('test1')->_get()->last());
    }
}