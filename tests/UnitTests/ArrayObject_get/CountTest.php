<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_get;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class CountTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(2, Data::array(['test', 'me'])->_get()->count());

        $array = [
            'test1' => ['me1'],
            'test2' => ['me2'],
        ];

        $this->assertEquals(2, Data::array($array)->_get()->count());

        $this->assertEquals(1, Data::array($array)->select('test1')->_get()->count());

        $this->assertEquals(1, Data::array($array)->selectOnce('test1')->_get()->count());

    }
}