<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_get;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ByKeyOrKeysTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals('test', Data::array(['test', 'me'])->_get()->byKeyOrKeys(0));

        $this->assertEquals(
            Data::array(['test', 'me'])->select(0)->return(),
            Data::array(['test', 'me'])->_get()->byKeyOrKeys(0)
        );
    }

    public function testNewInstance(): void
    {
        $array = [
            'test1' => ['me1'],
            'test2' => ['me2'],
        ];

        $this->assertEquals((new ArrayObject(['me1'])), Data::array($array)->_get()->byKeyOrKeys('test1'));
    }

    public function testArray(): void
    {
        $array = [
            'test1' => ['me1'],
            'test2' => ['me2'],
        ];

        $this->assertEquals(['me1'], Data::array($array)->_get()->byKeyOrKeys('test1', false));
    }

    public function testNewInstanceWithSelector(): void
    {
        $array = [
            'test1' => ['me1'],
            'test2' => ['me2'],
        ];

        $this->assertEquals('me1', Data::array($array)->select('test1')->_get()->byKeyOrKeys(0));
    }
}