<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class DebugSelectorTest extends TestCase
{
    public function testDefault(): void
    {
        // @todo Helper->getExpectedOutputFromFile()
        $expectedOutput = file_get_contents('tests/data/UnitTests/ArrayObject/DebugSelector/output/default');
        $eLines = explode(PHP_EOL, $expectedOutput);

        // @todo Use Helper->collectOutput()
        ob_start();
        Data::array(['test', 'me'])->select(1)->debugSelector();
        $output = ob_get_contents();
        file_put_contents('test.me', $output);
        ob_end_clean();
        $oLines = explode(PHP_EOL, $output);


        $this->assertEquals(array_slice($eLines, 1), array_slice($oLines, 1));
    }
}