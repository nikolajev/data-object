<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class Var_exportTest extends TestCase
{
    public function testDefault(): void
    {
        // @todo Helper->getExpectedOutputFromFile()
        $expectedOutput = file_get_contents('tests/data/UnitTests/ArrayObject/Var_export/output/default');

        $varExport = Data::array(['test', 'me'])->select(1)->var_export();

        $this->assertEquals($expectedOutput, $varExport);
    }

    public function testDefaultFormatting(): void
    {
        // @todo Helper->getExpectedOutputFromFile()
        $expectedOutput = file_get_contents('tests/data/UnitTests/ArrayObject/Var_export/output/defaultFormatting');

        $varExport = Data::array(['test', 'me'])->select(1)->var_export(false);

        $this->assertEquals($expectedOutput, $varExport);
    }

    // @todo + singleLine option (Params required)
    public function testSquareBrackets_ShowIndexes(): void
    {
        // @todo Helper->getExpectedOutputFromFile()
        $expectedOutput = file_get_contents('tests/data/UnitTests/ArrayObject/Var_export/output/SquareBrackets_ShowIndexes');

        $varExport = Data::array(['test', 'me'])->select(1)->var_export(true, true);

        $this->assertEquals($expectedOutput, $varExport);
    }


}