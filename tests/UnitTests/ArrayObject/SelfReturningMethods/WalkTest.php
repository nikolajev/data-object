<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class WalkTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(
            [
                'a' => 1,
                'b' => 2,
                '999' => "_999",
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                '999' => 999,
            ])->walk(function (&$value, $key) {
                if (is_numeric($key)) {
                    $value = "_$value";
                }
            })->return()
        );
    }

    public function testUnset(): void
    {
        $this->assertEquals(
            [
                'a' => 1,
                '999' => 999,
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                '999' => 999,
            ])->walk(function ($value) {
                if ($value === 2) {
                    return ArrayObject::WALK__UNSET;
                }
            })->return()
        );


        $total = 0;

        $this->assertEquals(
            [1, 3, 5],
            Data::array([1, 2, 3, 4, 5])->walk(function ($value) use (&$total) {
                if ($value % 2 === 0) {
                    return ArrayObject::WALK__UNSET;
                }
                $total += $value;
            })->return()
        );

        $this->assertEquals(9, $total); // 1+3+5
    }

    public function testUpdateKey()
    {
        $this->assertEquals(
            [
                'a' => 1,
                '_b' => 2,
                '999' => 999,
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                '999' => 999,
            ])->walk(function ($value, $key) {
                if ($value === 2) {
                    return [ArrayObject::WALK__UPDATE_KEY, "_$key"];
                }
            })->return()
        );
    }

    public function testBreak()
    {
        $this->assertEquals(
            [
                'a' => "_1",
                'b' => "_2",
                '999' => 999,
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                '999' => 999,
            ])->walk(function (&$value, $key) {
                $value = "_$value";

                if ($key === 'b') {
                    return ArrayObject::WALK__BREAK;
                }

                return null; // optional
            })->return()
        );
    }

    // @todo Replace with + Rename key
    public function testReplaceWith()
    {
        $this->assertEquals(
            [
                'a' => "_1",
                'b' => "test",
                '999' => "_999",
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                '999' => 999,
            ])->walk(function (&$value, $key) {
                $value = "_$value";

                if ($key === 'b') {
                    return [ArrayObject::WALK__REPLACE_WITH, 'test'];
                }

                return null; // optional
            })->return()
        );
    }


    public function testReplaceWithMultiple_()
    {
        $this->assertEquals(
            [
                'a' => "_1",
                '0' => "test",
                '1' => "me",
                'c' => "_3",
                '999' => "_999",
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                'c' => 3,
                '999' => 999,
            ])->walk(function (&$value, $key) {
                $value = "_$value";

                if ($key === 'b') {
                    // @todo Only sequential is allowed
                    return [ArrayObject::WALK__REPLACE_WITH_MULTIPLE, ['test', 'me']];
                }

                return null; // optional
            })->return()
        );
    }


    // @todo Test with array values
    // @todo Allow assoc arrays as well! LATER
    public function testReplaceWithMultiple2Times()
    {
        $this->assertSame(
            [
                'a' => "_1",
                '0' => "test",
                '1' => "me",
                '2' => "please",
                '3' => "thank",
                '4' => "you",
                '999' => "_999",
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                'c' => 3,
                '999' => 999,
            ])->walk(function (&$value, $key) {
                $value = "_$value";

                if ($key === 'b') {
                    return [ArrayObject::WALK__REPLACE_WITH_MULTIPLE, ['test', 'me']];
                }

                if ($key === 'c') {
                    return [ArrayObject::WALK__REPLACE_WITH_MULTIPLE, ['please', 'thank', 'you']];
                }

                return null; // optional
            })->return()
        );
    }

    public function testReplaceWithMultipleArrays2Times()
    {
        $this->assertSame(
            [
                'a' => "_1",
                '0' => ["test", 1],
                '1' => "me",
                'c' => "_3",
                '2' => "please",
                '3' => "thank",
                '4' => ["you", 2],
                '999' => "_999",
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                '999' => 999,
            ])->walk(function (&$value, $key) {

                $value = "_$value";

                if ($key === 'b') {
                    return [ArrayObject::WALK__REPLACE_WITH_MULTIPLE, [
                        ['test', 1],
                        'me'
                    ]];
                }

                if ($key === 'd') {
                    return [ArrayObject::WALK__REPLACE_WITH_MULTIPLE, [
                        'please',
                        'thank',
                        ['you', 2]
                    ]];
                }

                return null; // optional
            })->return()
        );
    }
}