<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class UnsetTest extends TestCase
{

    public function testDefault()
    {
        $this->assertEquals([
            1, 3
        ],
            Data::array([
                1, 2, 3
            ])
                ->unset(1)
                ->return()
        );

        $this->assertEquals([
            1, [2], 3
        ],
            Data::array([
                1,
                [1, 2],
                3
            ])
                ->unset([1, 0])
                ->return()
        );

        $this->assertEquals([
            1, [], 3
        ],
            Data::array([
                1,
                [1],
                3
            ])
                ->unset([1, 0])
                ->return()
        );
    }

    public function testUnsetEmptyParents()
    {
        $this->assertEquals([
            1, 3
        ],
            Data::array([
                1,
                [1],
                3
            ])
                ->unset([1, 0], true)
                ->return()
        );
    }


    public function testSelect()
    {
        $this->assertEquals([2],
            Data::array([
                1,
                [1, 2],
                3
            ])
                ->select(1)
                ->unset(0)
                ->return()
        );
    }

    public function testSelectOnce()
    {
        $this->assertEquals([
            1,
            [2],
            3
        ],
            Data::array([
                1,
                [1, 2],
                3
            ])
                ->selectOnce(1)
                ->unset(0)
                ->return()
        );
    }
}