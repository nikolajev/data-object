<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class SliceTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(
            [2, 3, 4],
            Data::array([1, 2, 3, 4, 5])
                ->slice(1, -1)
                ->return()
        );

    }
}