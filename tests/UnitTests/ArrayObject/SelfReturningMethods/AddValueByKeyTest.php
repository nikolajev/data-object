<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class AddValueByKeyTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(
            [1, 2],
            Data::array([1])
                ->addValueByKey(1, 2)
                ->return()
        );
    }

    public function testSelect(): void
    {

        $this->assertEquals(
            [2.1, 'second' => 2.2],
            Data::array([1, [2.1]])
                ->select(1)
                ->addValueByKey('second', 2.2)
                ->return()
        );
    }

    public function testSelectOnce(): void
    {

        $this->assertEquals(
            [1, [2.1, 'second' => 2.2]],
            Data::array([1, [2.1]])
                ->selectOnce(1)
                ->addValueByKey('second', 2.2)
                ->return()
        );
    }

}