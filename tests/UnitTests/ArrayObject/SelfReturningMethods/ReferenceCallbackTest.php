<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ReferenceCallbackTest extends TestCase
{
    public function testDefault()
    {
        // @todo Test select once
        $this->assertEquals([2],
            Data::array([
                'a' => 1,
                'b' => 1,
            ])
                ->referenceCallback(function (&$value) {
                    $value = [2];
                })
                ->return()
        );


        $this->expectExceptionMessage('Cannot assign int to reference held by property Nikolajev\DataObject\ArrayObject::$data of type array');

        Data::array([
            'a' => 1,
            'b' => 1,
        ])
            ->referenceCallback(function (&$value) {
                $value = 2; // Must be an array
            })
            ->return();
    }

    public function testReference()
    {
        $this->assertEquals([
            'a' => 1,
            'b' => 2,
        ],
            Data::array([
                'a' => 1,
                'b' => 1,
            ])
                ->referenceCallback(function (&$value) {
                    $value = 2;
                }, 'b')
                ->return()
        );
    }

    public function testSelectorAndReference()
    {
        $this->assertEquals([
            'a2' => 1,
            'b2' => 2,
        ],
            Data::array([
                'a1' => 1,
                'b1' => [
                    'a2' => 1,
                    'b2' => 1,
                ],
            ])
                ->select('b1')
                ->referenceCallback(function (&$value) {
                    $value = 2;
                }, 'b2')
                ->return()
        );
    }


    public function testSelectOnceAndReference()
    {
        $this->assertEquals([
            'a1' => 1,
            'b1' => [
                'a2' => 1,
                'b2' => 2,
            ],
        ],
            Data::array([
                'a1' => 1,
                'b1' => [
                    'a2' => 1,
                    'b2' => 1,
                ],
            ])
                ->selectOnce('b1')
                ->referenceCallback(function (&$value) {
                    $value = 2;
                }, 'b2')
                ->return()
        );
    }
}