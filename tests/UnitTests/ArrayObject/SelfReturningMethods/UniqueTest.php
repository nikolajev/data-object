<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class UniqueTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(
            ['one' => 1, 'three' => 3],
            Data::array(['one' => 1, 'two' => 1, 'three' => 3])
                ->unique()
                ->return()
        );

        $this->assertEquals(
            [1, 2, 3, 5],
            Data::array([1, 2, 3, 2, 5])
                ->unique()
                ->return()
        );

    }

    public function testArrays(): void
    {
        $this->assertEquals(
            ['one' => [1, 2], 'three' => 3],
            Data::array(['one' => [1, 2], 'two' => [1, 2], 'three' => 3])
                ->unique()
                ->return()
        );
    }
}