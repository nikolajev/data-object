<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ReindexTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(
            [1, 2 => 3, 4 => 5],
            Data::array([1, 2, 3, 4, 5])
                ->filter(function ($value) {
                    if ($value % 2 === 0) {
                        return false;
                    }

                    return true;
                })
                ->return()
        );

        $this->assertEquals(
            [1, 3, 5],
            Data::array([1, 2, 3, 4, 5])
                ->filter(function ($value) {
                    if ($value % 2 === 0) {
                        return false;
                    }

                    return true;
                })
                ->reindex()
                ->return()
        );
    }
}