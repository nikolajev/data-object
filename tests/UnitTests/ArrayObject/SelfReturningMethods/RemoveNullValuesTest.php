<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class RemoveNullValuesTest extends TestCase
{
    public function testDefault()
    {
        $this->assertEquals([
            'a' => 1,
            'c' => 3,
        ],
            Data::array([
                'a' => 1,
                'b' => null,
                'c' => 3,
            ])
                ->removeNullValues()
                ->return()
        );

        $this->assertEquals([
            'a' => 1,
            'c' => [
                1, 3
            ],
        ],
            Data::array([
                'a' => 1,
                'b' => null,
                'c' => [
                    1, null, 3
                ],
            ])
                ->removeNullValues()
                ->return()
        );

    }

    public function testNotRecursive()
    {
        $this->assertEquals([
            'a' => 1,
            'c' => [
                1, null, 3
            ],
        ],
            Data::array([
                'a' => 1,
                'b' => null,
                'c' => [
                    1, null, 3
                ],
            ])
                ->removeNullValues(false)
                ->return()
        );
    }
}