<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ValuesToOneTypeTest extends TestCase
{
    public function testString(): void
    {
        $this->assertSame([
            ["1.1", "1.2"],
            ["2.1", "2.2"],
            ["3.1", "3.2"],
            ["4.1", "4.2"],
            ["5.1", "5.2"]
        ],
            Data::array([
                ["1.1", "1.2"],
                ["2.1", "2.2"],
                [3.1, 3.2],
                [4.1, 4.2],
                [5.1, 5.2]
            ])
                ->valuesToOneType('string')
                ->return()
        );
    }

    public function testInteger(): void
    {
        $this->assertSame([
            [1, 1/* @todo 2 */],
            [2, 2],
            [3, 3/* @todo 4 */],
            [4, 4],
            [5, 5]
        ],
            Data::array([
                ["1.1", "1.5"],
                ["2.1", "2.2"],
                [3.1, 3.5],
                [4.1, 4.2],
                [5.1, 5.2]
            ])
                ->valuesToOneType('integer')
                ->return()
        );
    }

    public function testDouble(): void
    {
        $this->assertSame([
            [1.1, 1.2],
            [2.1, 2.2],
            [3.1, 3.2],
            [4.1, 4.2],
            [5.1, 5.2]
        ],
            Data::array([
                ["1.1", "1.2"],
                ["2.1", "2.2"],
                [3.1, 3.2],
                [4.1, 4.2],
                [5.1, 5.2]
            ])
                ->valuesToOneType('double')
                ->return()
        );
    }

    public function testBoolean(): void
    {
        $this->assertSame([
            [true],
            [false],
            [true],
            [false],
            [true],
            [false],
            [false],
            [false],
            [true],
            [false],
        ],
            Data::array([
                [1],
                [0],
                [true],
                [false],
                ['true'],
                ['false'],
                [''],
                [null],
                ['yes'],
                ['no'],
            ])
                ->valuesToOneType('boolean')
                ->return()
        );
    }

    public function testRecursion()
    {
        $this->assertSame([
            true,
            [true],
        ],
            Data::array([
                'true',
                ['true'],
            ])
                ->valuesToOneType('boolean')
                ->return()
        );

        $this->assertSame([
            true,
            ['true'],
        ],
            Data::array([
                'true',
                ['true'],
            ])
                ->valuesToOneType('boolean', false)
                ->return()
        );
    }
}