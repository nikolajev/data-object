<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class RecursiveWalkTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(
            [
                'a' => [
                    'b' => 'b',
                    '111' => '_111',
                ],
                '999' => "_999",
            ],
            Data::array([
                'a' => [
                    'b' => 'b',
                    '111' => '111',
                ],
                '999' => "999",
            ])->recursiveWalk(function (&$value, $key) {
                if (is_numeric($key)) {
                    $value = "_$value";
                }
            })->return()
        );
    }

    public function testUnset(): void
    {
        $this->assertEquals(
            [
                'a' => [1],
                '999' => [
                    'a' => [1],
                    'b' => 1,
                ]
            ],
            Data::array([
                'a' => [1, 2],
                'b' => 2,
                '999' => [
                    'a' => [1, 2],
                    'b' => 1,
                ],
            ])->recursiveWalk(function ($value) {
                if ($value === 2) {
                    return ArrayObject::WALK__UNSET;
                }
            })->return()
        );
    }

    public function testUpdateKey()
    {
        $this->assertEquals(
            [
                'a' => 1,
                '_b' => 2,
                '999' => 999,
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                '999' => 999,
            ])->recursiveWalk(function ($value, $key) {
                if ($value === 2) {
                    return [ArrayObject::WALK__UPDATE_KEY, "_$key"];
                }
            })->return()
        );

        $this->assertEquals(
            [
                'a' => [0 => 1, "_1" => 2],
                '_b' => 2,
                '999' => 999,
            ],
            Data::array([
                'a' => [1, 2],
                'b' => 2,
                '999' => 999,
            ])->recursiveWalk(function ($value, $key) {
                if ($value === 2) {
                    return [ArrayObject::WALK__UPDATE_KEY, "_$key"];
                }
            })->return()
        );
    }

    public function testBreak()
    {
        $this->assertEquals(
            [
                'a' => "_1",
                'b' => "_2",
                '999' => 999,
            ],
            Data::array([
                'a' => 1,
                'b' => 2,
                '999' => 999,
            ])->recursiveWalk(function (&$value, $key) {
                $value = "_$value";

                if ($key === 'b') {
                    return ArrayObject::WALK__BREAK;
                }

                return null; // optional
            })->return()
        );


        $this->assertEquals(
            [
                'a' => ["a" => "_1", "b" => "_2"],
                'b' => "2",
                '999' => 999,
            ],
            Data::array([
                'a' => ["a" => 1, "b" => 2],
                'b' => 2,
                '999' => 999,
            ])->recursiveWalk(function (&$value, $key) {
                if (is_array($value)) {
                    return;
                }

                $value = "_$value";

                if ($key === 'b') {
                    return ArrayObject::WALK__BREAK;
                }

                return null; // optional
            })->return()
        );
    }
}