<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use Nikolajev\Filesystem\File;
use PHPUnit\Framework\TestCase;

final class MergeTest extends TestCase
{
    public function testMergeMixed(): void
    {
        $a = Data::array()->merge([
            File::csv('test.csv', "1.1,1.2\n2.1,2.2"),
            new ArrayObject([
                [3.1, 3.2],
                [4.1, 4.2],
            ]),
            [
                [5.1, 5.2]
            ]
        ]);


        $this->assertEquals([
            ["1.1", "1.2"],
            ["2.1", "2.2"],
            [3.1, 3.2],
            [4.1, 4.2],
            [5.1, 5.2]
        ],
            $a->return()
        );

        // @todo Might be obsolete

        $this->assertEquals([
            [1.1, 1.2],
            [2.1, 2.2],
            [3.1, 3.2],
            [4.1, 4.2],
            [5.1, 5.2]
        ],
            $a->valuesToOneType('double')->return()
        );

        // @todo Might be obsolete

        $this->assertEquals([
            ["1.1", "1.2"],
            ["2.1", "2.2"],
            ["3.1", "3.2"],
            ["4.1", "4.2"],
            ["5.1", "5.2"]
        ],
            $a->valuesToOneType('string')->return()
        );
    }
}