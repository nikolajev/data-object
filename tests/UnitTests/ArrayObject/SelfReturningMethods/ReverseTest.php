<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ReverseTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(
            [5, 4, 3, 2, 1],
            Data::array([1, 2, 3, 4, 5])
                ->reverse()
                ->return()
        );

    }
}