<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\SelfReturningMethods;

use Nikolajev\DataObject\Data;
use Nikolajev\Debugger\Debugger;
use PHPUnit\Framework\TestCase;

final class RewriteTest extends TestCase
{
    private array $inputData = [
        'colors' => [
            'failure' => 'red',
            'success' => 'green',
        ],
        'codes' => [
            'failure' => 0,
            'success' => 1,
        ],
        'names' => [
            'Philipp',
            'Natalja',
            'Mia Maria',
        ]
    ];

    public function testDefault(): void
    {
        $this->assertEquals([
            'colors' => [
                'failure' => 'orange',
                'success' => 'green',
            ],
            'codes' => [
                'failure' => 0,
                'success' => '01',
            ],
            'names' => [
                'Philipp',
                'Natalja',
                'Mia Maria',
            ],
        ],
            Data::array($this->inputData)->rewrite([
                'colors' => [
                    'failure' => 'orange',
                ],
                'codes' => [
                    'success' => '01',
                ]
            ])->return()
        );
    }

    public function testException()
    {
        Debugger::silent(true);

        $this->expectExceptionMessage(
            'ArrayObject::rewrite() method\'s $dataToRewrite array structure is invalid: ' .
            '["Reference",["colors","failure"],["Expected type","string","Value before","red"],["Provided type","array","Provided value",["pink"]]]'
        );

        Data::array($this->inputData)->rewrite([
            'colors' => [
                'failure' => ['pink'],
            ],
        ]);

        Debugger::silent(false);
    }
}