<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class SelectTest extends TestCase
{
    public function testSequentialSingleSelector(): void
    {
        $this->assertEquals('me', Data::array(['test', 'me'])->select(1)->return());
    }

    public function testAssociatedNumericSingleSelector(): void
    {
        $this->assertEquals('me', Data::array(['test', 'me'])->select('1')->return());
    }

    public function testAssociatedSingleSelector(): void
    {
        $this->assertEquals('me', Data::array(['test' => 'me'])->select('test')->return());
    }

    public function testInvalidSequentialSingleSelector(): void
    {
        $this->expectExceptionMessage("Invalid reference");
        $this->assertEquals('me', Data::array(['test', 'me'])->select(2)->return());
    }

    public function testInvalidSequentialSingleSelector2(): void
    {
        $this->expectExceptionMessage("Invalid reference");
        $this->assertEquals('me', Data::array(['test' => 'me'])->select(0)->return());
    }

    public function testInvalidSequentialSingleSelector3(): void
    {
        $this->expectExceptionMessage("Invalid reference");
        $this->assertEquals('me', Data::array(['test' => 'me'])->select(1)->return());
    }

    public function testInvalidAssociatedSingleSelector(): void
    {
        $this->expectExceptionMessage("Invalid reference");
        $this->assertEquals('me', Data::array(['test' => 'me'])->select('anyKey')->return());
    }

    public function testSequentialSelector(): void
    {
        $this->assertEquals('me', Data::array([['test', 'me']])->select([0, 1])->return());
    }

    public function testInvalidSequentialSelector(): void
    {
        $this->expectExceptionMessage("Invalid reference");
        $this->assertEquals('me', Data::array([['test', 'me']])->select([0, 2])->return());
    }

    public function testMixedSelector(): void
    {
        $this->assertEquals('me', Data::array([['test' => 'me']])->select([0, 'test'])->return());
    }

    public function testInvalidMixedSelector(): void
    {
        $this->expectExceptionMessage("Invalid reference");
        $this->assertEquals('me', Data::array([['test' => 'me']])->select([1, 'test'])->return());
    }

    public function testInvalidMixedSelector2(): void
    {
        $this->expectExceptionMessage("Invalid reference");
        $this->assertEquals('me', Data::array([['test' => 'me']])->select([0, 'anyKey'])->return());
    }
}