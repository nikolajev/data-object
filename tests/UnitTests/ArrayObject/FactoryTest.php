<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class FactoryTest extends TestCase
{
    // @todo Separate test for Data::array(), ArrayObject does not have a method Factory (follow conventions)
    public function testConstructor(): void
    {
        $this->assertInstanceOf(ArrayObject::class, Data::array(['test', 'me']));
    }
}