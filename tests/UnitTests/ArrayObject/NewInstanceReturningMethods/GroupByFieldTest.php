<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\NewInstanceReturningMethods;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class GroupByFieldTest extends TestCase
{
    public function testDefault(): void
    {
        $data = [
            [1, 1, 1],
            [1, 2, 2],
            [3, 3, 3]
        ];

        $this->assertInstanceOf(ArrayObject::class, Data::array($data)->GroupByField(0));

        $this->assertEquals(
            [
                1 => [
                    [1, 1, 1],
                    [1, 2, 2],
                ],
                3 => [
                    [3, 3, 3]
                ]
            ],
            Data::array($data)->GroupByField(0)->return()
        );
    }


}