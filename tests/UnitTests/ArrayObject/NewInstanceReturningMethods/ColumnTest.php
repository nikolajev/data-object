<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\NewInstanceReturningMethods;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ColumnTest extends TestCase
{
    public function testDefault(): void
    {
        $data = [
            ['First 1', 'Second 1'],
            ['First 2', 'Second 2', 'Third 3']
        ];

        $this->assertInstanceOf(ArrayObject::class, Data::array($data)->Column(1));

        $this->assertEquals([
            'Second 1', 'Second 2'
        ],
            Data::array($data)->Column(1)->return()
        );
    }

    public function testReference(): void
    {
        $data = [
            [
                ['First 1', 'Second 1'],
            ],
            [
                ['First 2', 'Second 2', 'Third 3']
            ],
        ];

        $this->assertInstanceOf(ArrayObject::class, Data::array($data)->Column([0, 1]));

        $this->assertEquals([
            'Second 1', 'Second 2'
        ],
            Data::array($data)->Column([0, 1])->return()
        );
    }
}