<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\NewInstanceReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class DiffExtraTest extends TestCase
{
    // @todo Test assoc arrays
    public function testDefault(): void
    {
        $this->assertEquals(['me'], Data::array(['test', 'me'])->DiffExtra(['test'])->return());
        $this->assertEquals(['me'], Data::array(['test', 'me'])->DiffExtra(Data::array(['test']))->return());


        $this->assertEquals([], Data::array(['test'])->DiffExtra(['test', 'me'])->return());
    }
}