<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\NewInstanceReturningMethods;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ValuesTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertInstanceOf(ArrayObject::class, Data::array(['test' => 'me'])->Values());

        $this->assertEquals(['me'], Data::array(['test' => 'me'])->Values()->return());
    }
}