<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\NewInstanceReturningMethods;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class KeysTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertInstanceOf(ArrayObject::class, Data::array(['test' => 'me'])->Keys());

        $this->assertEquals(['test'], Data::array(['test' => 'me'])->Keys()->return());
    }
}