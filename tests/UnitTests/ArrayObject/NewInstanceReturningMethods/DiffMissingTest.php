<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\NewInstanceReturningMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class DiffMissingTest extends TestCase
{
    // @todo Test assoc arrays
    public function testDefault(): void
    {
        $this->assertEquals(['me'], Data::array(['test'])->DiffMissing(['test', 'me'])->return());
        $this->assertEquals(['me'], Data::array(['test'])->DiffMissing(Data::array(['test', 'me']))->return());


        $this->assertEquals([], Data::array(['test', 'me'])->DiffMissing(['test'])->return());
    }
}