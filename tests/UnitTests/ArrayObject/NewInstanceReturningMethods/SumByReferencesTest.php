<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\NewInstanceReturningMethods;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class SumByReferencesTest extends TestCase
{
    public function testDefault(): void
    {
        $data = [
            ['EUR', 120],
            ['EUR', 35],
            ['USD', 100],
        ];
        $this->assertIsInt(Data::array($data)->SumByReferences(1));

        $this->assertEquals(255, Data::array($data)->SumByReferences(1));


        $data = [
            ['EUR', 120.1],
            ['EUR', 35.25],
            ['USD', 100.15],
        ];
        $this->assertIsFloat(Data::array($data)->SumByReferences(1));

        $this->assertEquals(255.5, Data::array($data)->SumByReferences(1));


        $data = [
            ['data' => ['asset' => 'EUR', 'amount' => 120]],
            ['data' => ['asset' => 'EUR', 'amount' => 35]],
            ['data' => ['asset' => 'USD', 'amount' => 100]],
        ];

        $this->assertEquals(255, Data::array($data)->SumByReferences(['data', 'amount']));
    }

    public function testMultiple(): void
    {
        $data = [
            ['data' => ['asset' => 'EUR', 'amount' => 120]],
            ['data' => ['asset' => 'EUR', 'amount' => 35]],
            ['data' => ['asset' => 'USD', 'amount' => 100]],
        ];

        $this->assertInstanceOf(
            ArrayObject::class,
            Data::array($data)->SumByReferences(['data', 'amount'], ['data', 'asset'])
        );

        $this->assertEquals([
            'EUR' => 155,
            'USD' => 100,
        ],
            Data::array($data)->SumByReferences(['data', 'amount'], ['data', 'asset'])->return()
        );
    }

    public function testException()
    {
        $this->expectExceptionMessage("No negative amounts allowed: -120");

        $data = [
            ['EUR', -120],
        ];

        Data::array($data)->SumByReferences(1, null, true);
    }
}