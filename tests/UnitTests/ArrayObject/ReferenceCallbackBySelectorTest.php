<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ReferenceCallbackBySelectorTest extends TestCase
{
    public function testDefault()
    {
        $a = Data::array([
            'a' => 1,
            'b' => 1,
        ]);

        $a->referenceCallbackBySelector(function (&$value) {
            $value = [2];
        });

        $this->assertEquals([2], $a->return());


        $this->expectExceptionMessage('Cannot assign int to reference held by property Nikolajev\DataObject\ArrayObject::$data of type array');

        Data::array([
            'a' => 1,
            'b' => 1,
        ])->referenceCallbackBySelector(function (&$value) {
            $value = 2; // Must be an array
        });
    }

    public function testSelector()
    {
        $a = Data::array([
            'a' => 1,
            'b' => 1,
        ]);

        $b = clone $a;

        $a->select('b')->referenceCallbackBySelector(function (&$value) {
            $value = 2;
        });

        $b->selectOnce('b')->referenceCallbackBySelector(function (&$value) {
            $value = 2;
        });


        $this->assertEquals([
            'a' => 1,
            'b' => 2,
        ],
            $a->select()->return(),
        );

        $this->assertEquals([
            'a' => 1,
            'b' => 2,
        ],
            $b->return(),
        );
    }
}