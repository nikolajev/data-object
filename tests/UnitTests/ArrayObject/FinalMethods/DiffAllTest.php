<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\FinalMethods;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class DiffAllTest extends TestCase
{
    // @todo Test assoc arrays
    public function testDefault(): void
    {
        list($extra, $missing) = Data::array(['test', 'me'])->diffAll(['test', 'please']);

        $this->assertEquals(['me'], $extra);
        $this->assertEquals(['please'], $missing);
    }
}