<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\Filesystem\File;
use PHPUnit\Framework\TestCase;

final class BackToFileTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertInstanceOf(
            File\FileBase::class,
            File::csv('/test.csv', "1.1,1.2\n2.1,2.2")->toArrayObject()->backToFile()
        );

        $this->assertEquals(
            [
                ["1.1", "1.2"],
            ],
            File::csv('/test.csv', "1.1,1.2\n2.1,2.2")
                ->toArrayObject()
                ->unset(1)
                ->backToFile()
                ->toArray()
        );
    }
}