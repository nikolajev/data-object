<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ReturnTest extends TestCase
{
    public function testReturn(): void
    {
        $this->assertIsArray(Data::array(['test', 'me'])->return());
    }
}