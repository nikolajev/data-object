<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class DebugSelectedTest extends TestCase
{
    public function testDefault(): void
    {
        // @todo Helper->getExpectedOutputFromFile()
        $expectedOutput = file_get_contents('tests/data/UnitTests/ArrayObject/DebugSelected/output/default');
        list($eLine1, $eLine2) = explode(PHP_EOL, $expectedOutput);


        ob_start();
        Data::array(['test', 'me'])->select(1)->debugSelected();
        $output = ob_get_contents();
        //file_put_contents('test.me', $output);
        ob_end_clean();
        list($oLine1, $oLine2) = explode(PHP_EOL, $output);


        $this->assertEquals($eLine2, $oLine2);
    }
}