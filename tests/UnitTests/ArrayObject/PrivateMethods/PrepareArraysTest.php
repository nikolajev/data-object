<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject\PrivateMethods;

use PHPUnit\Framework\TestCase;

final class PrepareArraysTest extends TestCase
{
    public function testPrepareArrays(): void
    {
        $this->markTestSkipped('Use MergeTest::testMergeMixed()');
    }
}