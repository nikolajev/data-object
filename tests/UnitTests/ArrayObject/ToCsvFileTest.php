<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\DataObject\Data;
use Nikolajev\Filesystem\File;
use PHPUnit\Framework\TestCase;

final class ToCsvFileTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(
            File::csv('/test.csv', "1.1,1.2"),

            Data::array([
                ["1.1", "1.2"],
                ["2.1", "2.2"],
            ])->unset(1)->toCsvFile('/test.csv')
        );
    }
}