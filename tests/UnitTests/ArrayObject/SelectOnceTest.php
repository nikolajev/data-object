<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class SelectOnceTest extends TestCase
{
    public function testSelectOnce(): void
    {
        $Array = Data::array([
            'test' => [
                'test1' => 'me1',
                'test2' => 'me2'
            ]
        ]);

        $a = clone $Array;
        $a->select('test')->unset('test2');
        $this->assertEquals(['test1' => 'me1'], $a->return());
        $this->assertEquals(['test' => ['test1' => 'me1']], $a->select()->return());

        $b = clone $Array;
        $b->selectOnce('test')->unset('test2');
        $this->assertEquals(['test' => ['test1' => 'me1']], $b->return());

    }
}