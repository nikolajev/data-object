<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_validate;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class KeyExistsTest extends TestCase
{
    public function testDefault(): void
    {
        $array = [
            'test1' => ['me1'],
            'test2' => ['me2'],
        ];

        $this->assertEquals(true, Data::array($array)->_validate()->keyExists('test1'));

        $this->assertEquals(false, Data::array($array)->_validate()->keyExists('test'));
        $this->assertEquals(false, Data::array($array)->_validate()->keyExists(1));
    }
}