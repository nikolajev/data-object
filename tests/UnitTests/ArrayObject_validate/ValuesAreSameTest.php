<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_validate;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ValuesAreSameTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(false, Data::array(['test', 'me'])->_validate()->valuesAreSame());
        $this->assertEquals(false, Data::array([1, true])->_validate()->valuesAreSame());

        $this->assertEquals(true, Data::array(['test', 'test'])->_validate()->valuesAreSame());
        $this->assertEquals(true, Data::array([null, null])->_validate()->valuesAreSame());
    }

    public function testException(): void
    {
        $this->expectExceptionMessage("Value is not array");
        Data::array(['test', 'me'])->select(0)->_validate()->valuesAreSame();
    }
}