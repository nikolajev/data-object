<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_validate;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class IsAssocTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(false, Data::array(['test', 'me'])->_validate()->isAssoc());

        $array = [
            'test1' => ['me1'],
            'test2' => ['me2'],
        ];

        $this->assertEquals(true, Data::array($array)->_validate()->isAssoc());

        $this->assertEquals(false, Data::array($array)->select('test1')->_validate()->isAssoc());

        $this->assertEquals(false, Data::array($array)->selectOnce('test1')->_validate()->isAssoc());

    }
}