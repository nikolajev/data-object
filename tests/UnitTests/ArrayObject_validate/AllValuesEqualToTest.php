<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_validate;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class AllValuesEqualToTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertTrue(
            Data::array([
                [], []
            ])->_validate()->allValuesEqualTo([])
        );

        $this->assertTrue(
            Data::array([
                [], array()
            ])->_validate()->allValuesEqualTo([])
        );
    }
}