<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_validate;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class IsEmptyTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(false, Data::array(['test', 'me'])->_validate()->isEmpty());

        $this->assertEquals(true, Data::array([])->_validate()->isEmpty());

        $this->assertEquals(true, Data::array()->_validate()->isEmpty());

        $this->assertEquals(true, Data::array(['test' => []])->selectOnce('test')->_validate()->isEmpty());
    }
}