<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_validate;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ValuesAreUniqueTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertTrue(Data::array(['test', 'me'])->_validate()->valuesAreUnique());
        $this->assertFalse(Data::array(['test', 'me', 'test'])->_validate()->valuesAreUnique());

        $this->assertTrue(Data::array(['test' => [1, 2]])->select('test')->_validate()->valuesAreUnique());
        $this->assertFalse(Data::array(['test' => [1, 2, 1]])->select('test')->_validate()->valuesAreUnique());
    }
}