<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_validate;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class IsSimilarTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertTrue(
            Data::array(['test', 'me'])->_validate()->isSimilar(['test', 'me'])
        );

        $this->assertTrue(
            Data::array(['test', 'me'])->_validate()->isSimilar(['me', 'test'])
        );

        $this->assertFalse(
            Data::array(['test', 'me'])->_validate()->isSimilar(['me1', 'test'])
        );
    }
}