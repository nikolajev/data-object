<?php declare(strict_types=1);

namespace Tests\UnitTests\ArrayObject_validate;

use Nikolajev\DataObject\Data;
use PHPUnit\Framework\TestCase;

final class ValuesOfTypeTest extends TestCase
{
    public function testDefault(): void
    {
        $this->assertEquals(true, Data::array([null, null])->_validate()->valuesOfType('NULL'));
        $this->assertEquals(true, Data::array(['test', 'me'])->_validate()->valuesOfType('string'));
        $this->assertEquals(true, Data::array([1, 2])->_validate()->valuesOfType('integer'));
        $this->assertEquals(true, Data::array([1.1, 2.1])->_validate()->valuesOfType('double'));
        $this->assertEquals(true, Data::array([true, false])->_validate()->valuesOfType('boolean'));
        $this->assertEquals(true, Data::array([
            [1],
            [2]
        ])->_validate()->valuesOfType('array'));
        $this->assertEquals(true, Data::array([Data::array(), Data::array()])->_validate()->valuesOfType('object'));
    }

    public function testExceptionInvalidType(): void
    {
        $this->expectExceptionMessage("Invalid type 'Invalid type'");
        Data::array(['test', 'me'])->_validate()->valuesOfType('Invalid type');
    }

    public function testExceptionValueIsNotArray(): void
    {
        $this->expectExceptionMessage("Value is not array");
        Data::array(['test', 'me'])->select(0)->_validate()->valuesOfType('string');
    }

    public function testExceptionTypeDoesNotMatch(): void
    {
        $this->expectExceptionMessage("Type 'integer' expected, actual type is 'double'");
        $this->assertEquals(true, Data::array([1.1, 2.1])->_validate()->valuesOfType('integer'));
    }
}