<?php

namespace Nikolajev\DataObject\Traits\ArrayObject;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;

trait NewInstanceReturningMethods
{
    // @todo toValues() ???
    public function Values(): ArrayObject
    {
        return $this->referenceCallbackBySelector(function ($value) {
            return new ArrayObject(array_values($value));
        });
    }

    public function Keys(): ArrayObject
    {
        return $this->referenceCallbackBySelector(function ($value) {
            return new ArrayObject(array_keys($value));
        });
    }

    public function DiffExtra(mixed $arrayLikeInput): ArrayObject
    {
        $arrayToCompare = $this->prepareArray($arrayLikeInput);

        return $this->referenceCallbackBySelector(function ($value) use ($arrayToCompare) {
            return new ArrayObject(array_values(array_diff($value, $arrayToCompare)));
        });
    }

    public function DiffMissing(mixed $arrayLikeInput): ArrayObject
    {
        $arrayToCompare = $this->prepareArray($arrayLikeInput);

        return $this->referenceCallbackBySelector(function ($value) use ($arrayToCompare) {
            return new ArrayObject(array_values(array_diff($arrayToCompare, $value)));
        });
    }

    public function GroupByField(int|string $fieldKey): ArrayObject
    {
        return $this->referenceCallbackBySelector(function ($destination) use ($fieldKey) {
            $grouped = [];

            (new ArrayObject($destination))
                ->walk(function ($value) use ($fieldKey, &$grouped) {
                    $groupTitle = $value[$fieldKey];
                    $grouped[$groupTitle][] = $value;
                });


            return new ArrayObject($grouped);
        });
    }


    public function Column(int|string|array $keyOrKeys): ArrayObject
    {
        return $this->referenceCallbackBySelector(function ($destination) use ($keyOrKeys) {
            if (!is_array($keyOrKeys)) {
                return new ArrayObject(array_column($destination, $keyOrKeys));
            }

            $result = [];

            Data::array($destination)
                ->walk(function ($value) use ($keyOrKeys, &$result) {
                    $result[] = Data::array($value)->_get()->byKeyOrKeys($keyOrKeys);
                });

            return new ArrayObject($result);
        });
    }

    public function SumByReferences(
        int|string|array      $valueToSum_reference,
        null|int|string|array $groupByValue_reference = null,
                              $positiveValuesOnly = false
    ): int|float|ArrayObject
    {
        return $this->referenceCallbackBySelector(
            function ($destination)
            use ($valueToSum_reference, $groupByValue_reference, $positiveValuesOnly) {

                $result = $groupByValue_reference === null ? 0 : [];

                Data::array($destination)
                    ->walk(function ($value) use ($valueToSum_reference, $groupByValue_reference, $positiveValuesOnly, &$result) {

                        $amount = (new ArrayObject($value))->_get()->byKeyOrKeys($valueToSum_reference);

                        if ($positiveValuesOnly && $amount < 0) {
                            // @todo Debugger::isSilent()
                            throw new \Exception("No negative amounts allowed: $amount");
                        }

                        if ($groupByValue_reference === null) {
                            // @todo Use Math
                            $result += $amount;
                            return;
                        }

                        $key = (new ArrayObject($value))->_get()->byKeyOrKeys($groupByValue_reference);

                        if (!isset($result[$key])) {
                            $result[$key] = 0;
                        }

                        // @todo Use Math
                        $result[$key] += $amount;
                    });

                return $groupByValue_reference === null ? $result : new ArrayObject($result);
            }
        );
    }
}