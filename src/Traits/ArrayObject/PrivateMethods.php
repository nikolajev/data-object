<?php

namespace Nikolajev\DataObject\Traits\ArrayObject;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\Filesystem\File\FileBase;
use Nikolajev\Filesystem\Interfaces\FileInterface;

trait PrivateMethods
{
    private function prepareSelector()
    {
        $selector = $this->selector;

        if ($this->refreshSelector) {

            $this->selector = null;
            $this->refreshSelector = false;
        }

        return $selector;
    }

    private function validateDestinationKey($destination, $key)
    {
        if (!array_key_exists($key, $destination)) {
            throw new \Exception('Invalid reference');
        }

    }

    private function _unset(array &$keys, bool $unsetEmptyOnly = true)
    {
        $result = &$this->data;
        $last = array_pop($keys);

        foreach ($keys as $key) {
            $result = &$result[$key];
        }

        if ($unsetEmptyOnly && !empty($result[$last])) {
            return;
        }


        $isAssoc = (new ArrayObject($result))->_validate()->isAssoc();

        unset($result[$last]);

        // Reindex only if array was sequential
        if (!$isAssoc) {
            $result = array_values($result);
        }
    }

    private function prepareAllReferences(array $dataToRewrite)
    {
        $currentReference = [];
        $references = [];

        (new ArrayObject($dataToRewrite))->recursiveWalk(function ($value, $key) use (&$currentReference, &$references) {
            $currentReference[] = $key;
            if (!is_array($value)) {
                $references[] = $currentReference;
                $currentReference = [];
            }
        });

        return $references;
    }

    private function prepareArray(mixed $item)
    {
        if (is_array($item)) {
            return $item;
        }

        if (is_object($item)) {
            if (is_subclass_of($item, FileBase::class)) {
                /** @var FileInterface $item */
                return $item->toArray();
            }

            if (get_class($item) === ArrayObject::class) {
                return $item->return();
            }
        }


        // @todo Or throw exception + create a separate method combining them all
        failure('ArrayObject::prepareArrays() item type is not supported');
        trace();
        debugexit($item);

    }

    // @todo Add a single array method prepareArray() and use it in a loop
    private function prepareArrays(array $arrays): array
    {
        $result = [];

        foreach ($arrays as $item) {
            $result[] = $this->prepareArray($item);
        }

        return $result;
    }

    private function returningReferenceCallback(callable $callback, int|string|array $keyOrKeys = null)
    {
        if ($keyOrKeys === null || $keyOrKeys === []) {
            $destination = &$this->data;
        } elseif (is_string($keyOrKeys) || is_int($keyOrKeys)) {
            $this->validateDestinationKey($this->data, $keyOrKeys);
            $destination = &$this->data[$keyOrKeys];
        } else {
            $destination = &$this->data;
            foreach ($keyOrKeys as $key) {
                $this->validateDestinationKey($destination, $key);

                $destination = &$destination[$key];
            }
        }

        return $callback($destination);
    }

    private function mergeReferences(int|string|array $keyOrKeys1 = null, int|string|array $keyOrKeys2 = null)
    {
        if (is_int($keyOrKeys1) || is_string($keyOrKeys1)) {
            $keyOrKeys1 = [$keyOrKeys1];
        }

        if (is_int($keyOrKeys2) || is_string($keyOrKeys2)) {
            $keyOrKeys2 = [$keyOrKeys2];
        }

        return array_merge($keyOrKeys1 ?? [], $keyOrKeys2 ?? []);
    }

    private function prepareUnusedIndexes(array $arrayToReindex, array $arrayToPrepareFor)
    {
        $ArrayToReindex = new ArrayObject($arrayToReindex);

        if ($ArrayToReindex->_validate()->isAssoc()) {
            throw new \Exception("Assoc arrays are not allowed with WALK__REPLACE_WITH_MULTIPLE");
        }

        $arrayKeysToCheck = array_keys($arrayToPrepareFor);

        $ArrayToReindex
            ->walk(function ($value, $key) use (&$arrayKeysToCheck) {

                $keyBefore = $key;

                while (in_array($key, $arrayKeysToCheck)) {
                    $key++;
                }

                if ($keyBefore !== $key) {
                    $arrayKeysToCheck[] = $key;
                    return [ArrayObject::WALK__UPDATE_KEY, $key];
                }

            });

        return $ArrayToReindex->return();
    }


    private function updateKeys(array $destination, array $updateKeysConfig)
    {
        $preResult = [];

        foreach ($destination as $dKey => $dValue) {
            $preResult[$dKey] = [$updateKeysConfig[$dKey] ?? null, $dValue];
        }

        //show($preResult);

        $result = [];

        foreach ($preResult as $key => $values) {
            list($newKey, $value) = $values;
            $newKey = $newKey ?? $key;
            $result[$newKey] = $value;
        }

        return $result;
    }

}