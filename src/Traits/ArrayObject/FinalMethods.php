<?php

namespace Nikolajev\DataObject\Traits\ArrayObject;

use Nikolajev\DataObject\ArrayObject;

trait FinalMethods
{
    // @todo ->get()->diffAll()
    public function diffAll(array|ArrayObject $arrayLikeInput): array
    {
        $arrayToCompare = $this->prepareArray($arrayLikeInput);

        $extra = null;
        $missing = null;

        $this->referenceCallbackBySelector(function ($value) use ($arrayToCompare, &$extra, &$missing) {
            $extra = array_values(array_diff($value, $arrayToCompare));
            $missing = array_values(array_diff($arrayToCompare, $value));
        });

        return [$extra, $missing];
    }

    // @todo ???
    public function findOneByReferenceValue()
    {

    }
}