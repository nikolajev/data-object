<?php

namespace Nikolajev\DataObject\Traits\ArrayObject;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\Debugger\Debugger;

trait SelfReturningMethods
{
    public function referenceCallback(callable $callback, int|string|array $keyOrKeys = null): self
    {
        $this->returningReferenceCallback($callback, $this->mergeReferences($this->prepareSelector(), $keyOrKeys));

        return $this;
    }

// @todo Use $this->selector??? Better to avoid, throw exception if $this->selector !== null (Better just warning)
// @todo NB!!! Use $this->selector
// @todo Cannot avoid (conventions)

// @todo Add parameter reindexIfSequential
    public function unset(int|string|array $keyOrKeys, bool $unsetEmptyParents = false): self
    {
        $keys = $this->mergeReferences($this->prepareSelector(), $keyOrKeys);


        $this->_unset($keys, false);

        if (!$unsetEmptyParents) {
            return $this;
        }

// Unset all parents if they are empty
        while (count($keys) > 0) {
            $this->_unset($keys);
        }

        return $this;
    }


    public function push(mixed $value): self
    {
        $this->referenceCallbackBySelector(function (&$destination) use ($value) {
            $destination[] = $value;
        });

        return $this;
    }

    public function addValueByKey(int|string $key, mixed $value): self
    {
        $this->referenceCallbackBySelector(function (&$destination) use ($key, $value) {
            $destination[$key] = $value;
        });

        return $this;
    }

    public function filter(callable $callback, int $mode = 0): self
    {
        $this->referenceCallbackBySelector(function (&$destination) use ($callback, $mode) {
            $destination = array_filter($destination, $callback, $mode);
        });

        return $this;
    }


    // @todo Add Depth parameter ???
    public function walk(callable $callback, mixed $arg = null): self
    {
        $this->referenceCallbackBySelector(function (&$destination) use ($callback, $arg) {

            $updateKeys = [];
            $break = false;

            $destinationCursorNextPosition = 0;

            $call = function (&$value, $key) use (&$destination, $callback, $arg, &$break, &$updateKeys, &$destinationCursorNextPosition) {

                $itemPosition = array_search($key, array_keys($destination));

                if ($break || $itemPosition < $destinationCursorNextPosition) {
                    return;
                }


                $destinationCursorNextPosition = $itemPosition + 1;


                $result = $callback($value, $key);

                if ($result === self::WALK__BREAK) {
                    $break = true;
                } elseif ($result === self::WALK__UNSET) {

                    unset($destination[$key]);
                    $destinationCursorNextPosition--;

                } elseif (is_array($result) && $result[0] === self::WALK__UPDATE_KEY) {
                    $updateKeys[$key] = $result[1];
                } elseif (is_array($result) && $result[0] === self::WALK__REPLACE_WITH) {
                    $value = $result[1];
                } elseif (is_array($result) && $result[0] === self::WALK__REPLACE_WITH_MULTIPLE) {


                    $itemsToInsert = $this->prepareUnusedIndexes(
                        $result[1],
                        array_replace(
                            array_slice($destination, 0, $itemPosition, true),
                            array_slice($destination, $itemPosition + 1, null, true),
                        )
                    );

                    // Preserve keys (used instead of array_merge)
                    $destination = array_replace(
                        array_slice($destination, 0, $itemPosition, true),
                        $itemsToInsert,
                        array_slice($destination, ++$itemPosition, null, true),
                    );

                    $destinationCursorNextPosition += count($itemsToInsert) - 1;
                }
            };



            $destinationIsAssoc = (new ArrayObject($destination))->_validate()->isAssoc();

            $success = array_walk($destination, $call, $arg);

            // Reindex only if array is sequential
            if (!$destinationIsAssoc && count($updateKeys) === 0) {
                $destination = array_values($destination);
            }


            // @todo test
            if (!$success) {
                trace();
                failure('array_walk() failed');
            }

            $destination = $this->updateKeys($destination, $updateKeys);
        });

        return $this;
    }


    public function merge(array $arrays): self
    {
        $this->referenceCallbackBySelector(function (&$value) use ($arrays) {
            $value = call_user_func_array('array_merge', array_merge([$value], $this->prepareArrays($arrays)));
        });

        return $this;
    }


// @todo NB! Values before can be NULL!!! Test that it is like that
// @todo Use $this->selector???
    public function rewrite(array $dataToRewrite): self // @todo rewrite(): self|InvalidArrayObject (testing purposes)
    {
        $showExceptionDataCallback = function ($reference) use ($dataToRewrite) {

// @todo Prepare another exception context info if !array_key_exists()
            /**
             * 0   "Reference"
             * 1   []
             * 2   ["Expected type","array","Value before",{"colors":{"failure":"pink","success":"green"},"codes":{"failure":0,"success":"01"}}]
             * 3   ["Provided type","array","Provided value",{"test":"me"}]
             */

            array_pop($reference);

            $expectedType = null;
            $valueBefore = null;

            $this->returningReferenceCallback(function ($value) use (&$expectedType, &$valueBefore) {
                $expectedType = gettype($value);
                $valueBefore = $value;
            }, $reference);


            $actualType = null;
            $providedValue = null;

            (new ArrayObject($dataToRewrite))->returningReferenceCallback(function ($value) use (&$actualType, &$providedValue) {
                $actualType = gettype($value);
                $providedValue = $value;
            }, $reference);

            $exceptionFailureMessage = 'ArrayObject::rewrite() method\'s $dataToRewrite array structure is invalid';
            $exceptionMessageInfo = [
                'Reference',
                $reference,
                ['Expected type', $expectedType, 'Value before', $valueBefore],
                ['Provided type', $actualType, 'Provided value', $providedValue]
            ];

            if (Debugger::isSilent()) {
                throw new \Exception("$exceptionFailureMessage: " . json_encode($exceptionMessageInfo));
            }

            failure($exceptionFailureMessage);

            call_user_func_array('showln', $exceptionMessageInfo);

            // @todo Global config for testing purposes
            //trace();
            traceexit();
        };


        $references = $this->prepareAllReferences($dataToRewrite);

        // Check that references are valid
        try {
            foreach ($references as $reference) {

// @todo Reference callback should not allow indexed references that do not exist yet!
                $this->returningReferenceCallback(function ($value) {
                }, $reference);
            }

        } catch (\Error|\Exception $e) {


            if ($e->getMessage() === 'Invalid reference') {
                $showExceptionDataCallback($reference);

                return new ArrayObject; // @todo Try InvalidArrayObject extends ArrayObject + provide data that was before failure
            }

            try {
                for ($i = count($reference) - 1; $i >= 0; $i--) {
                    $this->returningReferenceCallback(function ($value) {
                    }, array_slice($reference, $i));
                }
            } catch (\Error|\Exception $e) {
                $showExceptionDataCallback($reference);
                return new ArrayObject; // @todo Try InvalidArrayObject extends ArrayObject + provide data that was before failure
            }
        }


        $this->data = array_merge_recursive($this->data, $dataToRewrite);

        foreach ($references as $reference) {
            $this->returningReferenceCallback(function (&$value) {
                $value = array_reverse($value)[0];
            }, $reference);
        }

        return $this;
    }

    // @todo Use break + tests
    public function recursiveWalk(callable $callback, mixed $arg = null, bool &$break = false): self
    {
        $finalCallback = function (&$value, $key) use ($callback, $arg, &$break) {

            if ($break) {
                return;
            }

            $result = $callback($value, $key);

            if ($result === self::WALK__BREAK) {
                $break = true;
                return;
            }

            if (!is_array($value)) {
                return $result;
            }

            $value = (new ArrayObject($value))->recursiveWalk($callback, $arg, $break)->return();
        };

        // @todo Test all self::walk() method's features (unset + update key + break)
        $this->walk($finalCallback);

        return $this;
    }

    public function slice(int $offset, int $length = null): self
    {
        $this->referenceCallbackBySelector(function (&$value) use ($offset, $length) {
            $value = array_slice($value, $offset, $length);
        });

        return $this;
    }

    public function reverse(): self
    {
        $this->referenceCallbackBySelector(function (&$value) {
            $value = array_reverse($value);
        });

        return $this;
    }

    public function reindex(): self
    {
        $this->referenceCallbackBySelector(function (&$value) {
            $value = array_values($value);
        });

        return $this;
    }


    public function removeNullValues(bool $recursive = true): self
    {
        $this->referenceCallbackBySelector(function (&$value) use ($recursive) {
            $walkMethod = $recursive ? 'recursiveWalk' : 'walk';
            $value = (new ArrayObject($value))->$walkMethod(function ($value2) {
                if ($value2 === null) {
                    return ArrayObject::WALK__UNSET;
                }
            })->return();
        });

        return $this;
    }

    public function valuesToOneType(string $type, bool $recursive = true): self
    {
        // @todo 'array', 'object' & 'resource' type + test
        if (!in_array($type, ['string', 'integer', 'double', 'boolean',/* 'array', 'object'*/], true)) {
            throw new \Exception("Invalid type '$type'");
        }

        $this->referenceCallbackBySelector(function (&$value) use ($type, $recursive) {
            $walkMethod = $recursive ? 'recursiveWalk' : 'walk';
            $value = (new ArrayObject($value))->$walkMethod(function (&$value2) use ($type) {
                if (is_array($value2)) {
                    return;
                }

                switch ($type) {
                    case 'string':
                        $value2 = (string)$value2;
                        break;
                    case 'integer':
                        $value2 = (int)$value2;
                        break;
                    case 'double':
                        $value2 = (float)$value2;
                        break;
                    case 'boolean':
                        $value2 = filter_var($value2, FILTER_VALIDATE_BOOLEAN);
                        break;
                }
            })->return();
        });

        return $this;
    }


    // @todo Add strict parameter (check types as well or ignore)
    public function unique(): self
    {
        $this->referenceCallbackBySelector(function (&$value) {
            $isAssoc = (new ArrayObject($value))->_validate()->isAssoc();

            $value = array_unique($value, SORT_REGULAR);

            if (!$isAssoc) {
                $value = array_values($value);
            }
        });

        return $this;
    }

    // @todo + ...WithReference
    public function sortByValues()
    {

    }

    // @todo + ...WithReference
    public function sortByKeys()
    {

    }
}