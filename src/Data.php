<?php

namespace Nikolajev\DataObject;

class Data
{
    public static function array(array $data = [])
    {
        return new ArrayObject($data);
    }
}