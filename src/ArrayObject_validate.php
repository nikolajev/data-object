<?php declare(strict_types=1);

namespace Nikolajev\DataObject;

class ArrayObject_validate
{
    private ArrayObject $ArrayObject;

    public function __construct(ArrayObject $ArrayObject)
    {
        $this->ArrayObject = $ArrayObject;
    }

    public function isAssoc(): bool|self // @todo Why self ?
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($value) {
            if ($value === []) return false;
            return array_keys($value) !== range(0, count($value) - 1);
        });
    }

    public function isEmpty()
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($value) {
            return empty($value);
        });
    }

    public function valuesAreSame()
    {
        $firstValueSet = false;
        $firstValue = null;
        $allValuesAreSame = true;

        $this->ArrayObject->referenceCallbackBySelector(function ($value) use (&$firstValueSet, &$firstValue, &$allValuesAreSame) {
            if (!is_array($value)) {
                throw new \Exception("Value is not array");
            }

            (new ArrayObject($value))->walk(function ($value) use (&$firstValueSet, &$firstValue, &$allValuesAreSame) {

                // @todo Break from array walk ???
                if ($allValuesAreSame === false) {
                    return;
                }

                if (!$firstValueSet) {
                    $firstValue = $value;
                    $firstValueSet = true;
                    return;
                }

                if ($firstValue !== $value) {
                    $allValuesAreSame = false;
                    // @todo Break from array walk ???
                }
            });
        });

        return $allValuesAreSame;
    }

    public function valuesOfType(string $type)
    {
        // @todo 'resource' type + test
        if (!in_array($type, ['NULL', 'string', 'integer', 'double', 'boolean', 'array', 'object'], true)) {
            throw new \Exception("Invalid type '$type'");
        }

        $firstValueTypeSet = false;
        $firstValueType = null;
        $allValuesTypesAreSame = true;

        $this->ArrayObject->referenceCallbackBySelector(function ($value) use (&$firstValueTypeSet, &$firstValueType, &$allValuesTypesAreSame, $type) {
            if (!is_array($value)) {
                throw new \Exception("Value is not array");
            }

            // @todo trace(); - Error: Object of class PHPUnit\Framework\TestSuite could not be converted to string [nikolajev/debugger]

            (new ArrayObject($value))->walk(function ($value) use (&$firstValueTypeSet, &$firstValueType, &$allValuesTypesAreSame, $type) {

                if (!$firstValueTypeSet) {
                    $firstValueType = gettype($value);
                    if ($firstValueType !== $type) {
                        throw new \Exception("Type '$type' expected, actual type is '$firstValueType'");
                    }
                    $firstValueTypeSet = true;
                    return;
                }

                if ($firstValueType !== gettype($value)) {
                    $allValuesTypesAreSame = false;
                    return ArrayObject::WALK__BREAK;
                }
            });
        });

        return $allValuesTypesAreSame;
    }

    // @todo Implement $sorttype from array_unique()
    public function valuesAreUnique()
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($value) {
            return count($value) === count(array_unique($value));
        });
    }

    public function allValuesEqualTo(mixed $valueToCompare): bool
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($destination) use ($valueToCompare) {
            $valueIsValid = true;

            (new ArrayObject($destination))
                ->walk(function ($value) use ($valueToCompare, &$valueIsValid) {
                    if ($value !== $valueToCompare) {
                        $valueIsValid = false;
                    }

                    if (!$valueIsValid) {
                        return ArrayObject::WALK__BREAK;
                    }
                });

            return $valueIsValid;
        });
    }

    public function isSimilar(array|ArrayObject $arrayLikeInput): bool
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($value) use ($arrayLikeInput) {
            $differences = (new ArrayObject($value))->diffAll($arrayLikeInput);

            // @todo ->noDiff($arrayLikeInput) ??? ->isSimilar() does exactly the same
            if ((new ArrayObject($differences))->_validate()->allValuesEqualTo([])) {
                return true;
            }

            return false;
        });
    }

    // @todo isMultidimensional()

    public function includes(mixed $input, bool $strict = false)
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($value) use ($input, $strict) {
            return in_array($input, $value, $strict);
        });
    }

    public function keyExists(int|string $key)
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($value) use ($key) {
            return array_key_exists($key, $value);
        });
    }
}