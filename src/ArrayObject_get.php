<?php declare(strict_types=1);

namespace Nikolajev\DataObject;

class ArrayObject_get
{
    private ArrayObject $ArrayObject;

    public function __construct(ArrayObject $ArrayObject)
    {
        $this->ArrayObject = $ArrayObject;
    }

    public function count(): int
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($value) {
            return count($value);
        });
    }

    // @todo Just $newInstance = true ??? This is more strict way, might be more safe
    public function first(bool $newInstanceIfPossible = true): mixed
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($value) use ($newInstanceIfPossible) {
            $result = array_values($value)[0] ?? null;

            if ($newInstanceIfPossible && is_array($result)) {
                return new ArrayObject($result);
            }

            return $result;
        });
    }

    public function last(bool $newInstanceIfPossible = true): mixed
    {
        return $this->ArrayObject->referenceCallbackBySelector(function ($value) use ($newInstanceIfPossible) {
            $result = array_reverse(array_values($value))[0] ?? null;

            if ($newInstanceIfPossible && is_array($result)) {
                return new ArrayObject($result);
            }

            return $result;
        });
    }

    public function byKeyOrKeys(int|string|array $keyOrKeys, bool $newInstanceIfPossible = true): mixed
    {
        $result = (new ArrayObject($this->ArrayObject->return()))->select($keyOrKeys)->return();

        if ($newInstanceIfPossible && is_array($result)) {
            return new ArrayObject($result);
        }

        return $result;
    }

}