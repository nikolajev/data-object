<?php declare(strict_types=1);

namespace Nikolajev\DataObject;

use Nikolajev\DataObject\Traits\ArrayObject\FinalMethods;
use Nikolajev\DataObject\Traits\ArrayObject\NewInstanceReturningMethods;
use Nikolajev\DataObject\Traits\ArrayObject\PrivateMethods;
use Nikolajev\DataObject\Traits\ArrayObject\SelfReturningMethods;
use Nikolajev\Filesystem\File\CsvFile;
use Nikolajev\Filesystem\File\FileBase;


// @todo $output = Data::collectReturned(Data::array([])->select(...)->walk(...)->_debugSelector()->validate()->_isAssoc()->_count());
class ArrayObject
{
    const WALK__UNSET = 'array_walk__unset';
    const WALK__UPDATE_KEY = 'array_walk__update_key';
    const WALK__BREAK = 'array_walk__break';
    const WALK__REPLACE_WITH = 'array_walk__replace_with';
    const WALK__REPLACE_WITH_MULTIPLE = 'array_walk__replace_with_multiple';

    private array $data;

    private bool $refreshSelector = false;

    private null|int|string|array $selector = null;

    private array $modificationHistory = [];

    use PrivateMethods;
    use SelfReturningMethods;
    use NewInstanceReturningMethods;
    use FinalMethods;

    // @todo Modification history + ModificationHistoryInterface


    // @todo difference()
    // !filter(), !walk(), !map(), !unset([], true), !save() [var_export], !->values()->first(), !->keys()->first(), !last()
    // ?append(), ?prepend(), !merge(), !rewriteMerge(), !mergeRecursive()
    // !add()
    // !slice()
    // unsetNullValues()
    // !count()
    // ->validate()->allValuesAreSame(): bool (etc.) + ->allValuesEqual(true)

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    // @todo DRY (separate trait)
    public function setModifications(array $modifications = []): self
    {
        $this->modificationHistory = $modifications;

        return $this;
    }

    // @todo DRY (separate trait)
    public function addModification(object $object): self
    {
        $this->modificationHistory[] = $object;

        return $this;
    }

    // @todo continueSelect()
    // @todo backToSelector() ??? $a->select(...)->first()->last()->backToSelector() - NO, no need!
    // @todo ->first() & ->last() - this methods modify selector
    // @todo ->values() & ->keys() - this methods DO NOT modify selector
    // $a->values()->first()->backToArray()
    // @todo first() + _first()
    // @todo last() + _last() - first can return ArrayObject, second one array or something else + writes to Data::collectReturned()
    // @todo backToFile() + backToFirstFile() + backToArrayObject() + backToFirstArrayObject()

    public function select(int|string|array $keyOrKeys = null): self
    {
        $this->selector = $keyOrKeys;

        return $this;
    }

    // @todo continueSelectOnce()
    public function selectOnce(int|string|array $keyOrKeys = null): self
    {
        $this->selector = $keyOrKeys;
        $this->refreshSelector = true;

        return $this;
    }

    public function debugSelected(string $outputType = 'show'): self
    {
        $this->referenceCallbackBySelector(function ($value) use ($outputType) {

            switch ($outputType) {
                case "show":
                    // @todo Improve caller info, get caller for debugSelected()
                    // Set caller key (0+) to any value if needed + set defaults to ?
                    show($value);
                    break;
                case "debug":
                    // @todo Improve caller info, get caller for debugSelected()
                    // Set caller key (0+) to any value if needed + set defaults to ?
                    debug($value);
                    break;
                case "showln":
                    // @todo Improve caller info, get caller for debugSelected()
                    // Set caller key (0+) to any value if needed + set defaults to ?
                    showln($value);
                    break;
                default:
                    throw new \Exception("Unsupported output type '$outputType'");
            }
        });

        return $this;
    }

    public function debugSelector(): self
    {
        showln('Selector', $this->selector, 'Refresh selector', $this->refreshSelector);
        return $this;
    }

    // @todo __return() ???
    // @todo NB!!! Obsolete! just use toArray() ???
    public function return(): mixed
    {
        return $this->referenceCallbackBySelector(function ($value) {
            return $value;
        });
    }

    // @todo __var_export() ???
    public function var_export(bool $squareBrackets = true, bool $showIndexes = false)
    {
        if ($squareBrackets && !Data::array($this->data)->_validate()->isAssoc() && !$showIndexes) {
            // @todo return Json::encode($array);
            // @todo NB! This is not sos simple! Recursion required, same var_export() for each sub-element, complicated logics starting from very end
            return json_encode($this->data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }


        // @todo NB! Implement selector
        if (!$squareBrackets) {
            return var_export($this->data, true);
        }

        // @link https://gist.github.com/Bogdaan/ffa287f77568fcbb4cffa0082e954022
        $export = var_export($this->data, true);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [NULL, ']$1', ' => ['], $array);


        $export = join(PHP_EOL, array_filter(["["] + $array));

        return $export;
    }

    // @todo Add selector (nullable) If null, return ArrayObject_get ???
    public function _get(): ArrayObject_get
    {
        // @todo ->refreshSelector() setter
        // Cannot use '$this' or 'clone $this'
        return new ArrayObject_get((new ArrayObject($this->data))->select($this->selector));
    }


    public function _validate(): ArrayObject_validate
    {
        // @todo ->refreshSelector() setter
        // Cannot use '$this' or 'clone $this'
        return new ArrayObject_validate((new ArrayObject($this->data))->select($this->selector));
    }

    // @todo backToFirstFile()
    // @todo public function backToFile(int $backNTimes = null)
    public function backToFile()
    {
        foreach ($this->modificationHistory as $modificatedObject) {
            /** @var FileBase $modificatedObject */
            if (is_subclass_of($modificatedObject, FileBase::class)) {
                return $modificatedObject
                    ->setData($this->data)
                    ->setModifications($this->modificationHistory)
                    ->addModification($this); // @todo Improve by storing only required data
            }
        }
        // @todo throw e
    }

    // @todo ->toFile()->csv(string $title) - toFile() is just a Factory
    public function toCsvFile(string $title)
    {
        return (new CsvFile($title))->setData($this->data);
    }

    // @todo Test
    public function referenceCallbackBySelector(callable $callback)
    {
        // @todo caller()->filePath(); + caller()->line(); for debugging purposes (nikolajev/debugger)
        return $this->returningReferenceCallback($callback, $this->prepareSelector());
    }
}